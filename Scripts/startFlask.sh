#!/bin/sh
#Usage source startFlask.sh
cd ../flaskProject
readonly sourceFile="../flaskProject/venv/bin/activate"
source ${sourceFile}
export FLASK_APP=run.py
export FLASK_ENV=development
flask run