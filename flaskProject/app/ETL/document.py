class Document(object):
    condition = "New"
    def __init__(self, docId, language="EN-GB", year=2018, fullText=""):
        self.docId = docId
        self.language = language
        self.year = year
        self.fullText = fullText

    def toDBCollection(self):
        return {
            "docId": self.docId,
            "language": self.language,
            "year": self.year,
            "fullText": self.fullText
        }

