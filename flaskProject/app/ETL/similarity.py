from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk, string, logging,time


class textSimilarity():

    def __init__(self):
        self.lemmer = nltk.stem.WordNetLemmatizer()
        self.remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

#nltk.download('wordnet') # first-time use only


    def LemTokens(self,tokens):
        '''
        :param tokens:
        :return: returns the lemmatization from the tokens
        '''
        return [self.lemmer.lemmatize(token) for token in tokens]


    def LemNormalize(self,text):
        return self.LemTokens(nltk.word_tokenize(text.lower().translate(self.remove_punct_dict)))

    def getPandasList(self,df):
        # print(df[['docId', 'fullText']].reset_index().values.tolist())
        return df['fullText'].tolist()

    def cos_similarity(self,textlist):
        TfidfVec = TfidfVectorizer(tokenizer=self.LemNormalize, stop_words='english')
        tfidf = TfidfVec.fit_transform(textlist)
        return (tfidf * tfidf.T).toarray()

    def getCosineSimilarity(self,df,text, top):
        '''
        Function to get an array of the top most similar text, index and text to the text sent as parameter
        :param text:
        :param top:
        :return:
        '''
        train_str = text

        try:
            start = time.time()
            #logging.info('After getPandasList: getCosineSimilarity:' + str(start))
            # add the text to query in the first position of the training
            train_set = self.getPandasList(df)
            logging.info('train_set:' + str(train_set))
            train_set.insert(0, train_str)
            # Set up the vectoriser, passing in the stop words
            tfidf_vectorizer = TfidfVectorizer(stop_words='english')
            # Apply the vectoriser to the training set
            tfidf_matrix_train = tfidf_vectorizer.fit_transform(train_set)
            #logging.info('After tfidf_matrix_train: getCosineSimilarity:' + str(time.time()))
            cosine_similarities = cosine_similarity(tfidf_matrix_train[0:1], tfidf_matrix_train)
            #logging.info('After cosine_similarities: getCosineSimilarity:' + str(time.time()))
            result = [i for i in cosine_similarities[0].argsort()[::-1] if i != 0]
            end = time.time()
            #logging.info('After result: getCosineSimilarity:' + str(end - start))
            return [(index, cosine_similarities[0][index]) for index in result][0:top]
        except:
            return [{'index':-1, 'score':1, 'text':str('Error')}]


    def getCosineText(self,df,text, top):
        '''
        Function to get the top most similar documents to the text sent as parameter
        :param text:
        :param top:
        :return:
        '''
        start = time.time()
        #logging.info('Started: getCosineText:' + str(start))
        train_set = self.getPandasList(df)
        resultArray = []
        for index, score in self.getCosineSimilarity(df,text, top):
            #print(str({'index':str(index-1), 'score':str(score), 'text':str(train_set[index-1])}))
            resultArray.append({'docId':str(df.iloc[[index -1]]['docId'].iloc[0]),
                                'index':str(index-1),
                                'score':str(score),
                                'text':str(train_set[index-1])})

        return resultArray


    def checkParameters(self,df,text,top):
        return df.shape[0]