import os, multiprocessing,logging
import os,xml.etree.ElementTree as ET, logging, document
import dataBase
import nltk, string, numpy
arrfiles = []

class FileManager():
    def __init__(self,test=False):
        self.test = test
        if (not self.test):
            self.path = "../../../DATA"
        else:
            self.path = "../../../DATATEST"

    def showfileName(self,path):
        print(path)
        logging.info('FileS:' + path)
        #print(file)


    def loadFiles(self):
        arrfiles = [os.path.join(d, x)
                    for d, dirs, files in os.walk(self.path)
                for x in files if x.endswith(".tmx")]
    #arrTexts =[handleFile.handleFile(file)
    # for file in arrfiles]
        return arrfiles

    def myMultiprocessing(self):
        '''
        Function to parse every file in parallel processing using as much processors as the device has.
        :return:
        '''
        arrfiles = self.loadFiles()
        logging.info('Files loaded')
        processes = multiprocessing.cpu_count()
        logging.info('Number of processes:' + str(processes))

        pool = multiprocessing.Pool(processes)
        result = pool.map(self.handleFile,arrfiles)
        pool.close()
        pool.join()

    def handleFile(self,path):
        '''
        Function to parse and store every file in mongodb database
        :param path: file path
        :return: nothing
        '''
        # logging.info('HAndling path:' + path)
        year = path.split('/')[4].split('_')[1]
        #Only for the version deployed to heroku 
        # if (int(year) > 2006 or self.test):
        tree = ET.parse(path)
        docId = tree.find(".//prop[@type='Txt::Doc. No.']").text
        language = tree.find(".//tuv[@lang='EN-GB']").attrib['lang']
        arrText = tree.findall(".//tuv[@lang='EN-GB']/seg")
        fullText = ""
        for textFragment in arrText:
            fullText = ' '.join([fullText, textFragment.text])

        doc2Insert = document.Document(docId, language, int(year), fullText)
        mongoConn = dataBase.MongoConnection("localhost", 27017, self.test)
        mongoConn.insertDocument(doc2Insert.toDBCollection())
        return True