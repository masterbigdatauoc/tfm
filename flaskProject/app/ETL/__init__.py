import os
import glob
from flask import Blueprint
__all__ = [os.path.basename(f)[:-3]
    for f in glob.glob(os.path.dirname(__file__) + "/*.py")]

sim_bp = Blueprint('similarity', __name__)
db_bp = Blueprint('database',__name__)

from app.ETL import similarity
from app.ETL import dataBase