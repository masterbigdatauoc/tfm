import logging, time
import similarity, dataBase

def main():
    logging.basicConfig(filename='../../../log/myapp.log', level=logging.INFO)
    start = time.time()
    test = True
    logging.info('Started:' + str(start))
    mongoConnection = dataBase.MongoConnection("localhost", 27017, test)
    # dataFrame = mongoConnection.getRecords()
    dataFrame = mongoConnection.getRecords2Compare()
    text = "Decision of the EEA Joint Committee of 8 June 2004 amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement THE EEA JOINT COMMITTEE, Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as ‘the Agreement’, and in particular Article 98 thereof, Whereas: Annex II to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg [1]. Directive 2003/15/EC of the European Parliament and of the Council of 27 February 2003 amending Council Directive 76/768/EEC on the approximation of the laws of the Member States relating to cosmetic products [2] is to be incorporated into the Agreement, HAS DECIDED AS FOLLOWS: Article 1 The following indent shall be added in point 1 (Council Directive 76/768/EEC) of Chapter XVI of Annex II to the Agreement: Directive 2003/15/EC of the European Parliament and of the Council of 27 February 2003 (OJ L 66, 11.3.2003, p. 26).’ Article 2 The texts of Directive 2003/15/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic. Article 3 This Decision shall enter into force on 9 June 2004, provided that all the notifications pursuant to Article 103(1) of the Agreement have been made to the EEA Joint Committee [3]. Article 4 This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union. Done at Brussels, 8 June 2004. For the EEA Joint Committee The President Gillespie OJ L 130, 29.4.2004, p. 3. OJ L 66, 11.3.2003, p. 26. No constitutional requirements indicated"
    #text = "Commission Regulation (EC) No 263/2008 of 19 March 2008 granting no export refund for butter in the framework of the standing invitation to tender provided for in Regulation (EC) No 581/2004 THE COMMISSION OF THE EUROPEAN COMMUNITIES, Having regard to the Treaty establishing the European Community, Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular the third subparagraph of Article 31(3) thereof, Whereas: Commission Regulation (EC) No 581/2004 of 26 March 2004 opening a standing invitation to tender for export refunds concerning certain types of butter [2] provides for a permanent tender. Pursuant to Article 5 of Commission Regulation (EC) No 580/2004 of 26 March 2004 establishing a tender procedure concerning export refunds for certain milk products [3] and following an examination of the tenders submitted in response to the invitation to tender, it is appropriate not to grant any refund for the tendering period ending on 18 March 2008. The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products, HAS ADOPTED THIS REGULATION: Article 1 For the permanent tender opened by Regulation (EC) No 581/2004, for the tendering period ending on 18 March 2008 no export refund shall be granted for the products and destinations referred to in Article 1(1) of that Regulation. Article 2 This Regulation shall enter into force on 20 March 2008. This Regulation shall be binding in its entirety and directly applicable in all Member States. Done at Brussels, 19 March 2008. For the Commission Director-General for Agriculture and Rural Development OJ L 160, 26.6.1999, p. 48. Regulation as last amended by the Council (EC) No 1152/2007 (OJ L 258, 4.10.2007, p. 3). Regulation (EEC) No 1255/1999 will be replaced by Regulation (EC) No 1234/2007 (OJ L 299, 16.11.2007, p. 1) as from 1 July 2008. OJ L 90, 27.3.2004, p. 64. Regulation as last amended by Regulation (EC) No 1543/2007 (OJ L 337, 21.12.2007, p. 62). OJ L 90, 27.3.2004, p. 58. Regulation as last amended by Regulation (EC) No 128/2007 (OJ L 41, 13.2.2007, p. 6). OJ L 160, 26.6.1999, p. 48. Regulation as last amended by the Council (EC) No 1152/2007 (OJ L 258, 4.10.2007, p. 3). OJ L 90, 27.3.2004, p. 64. Regulation as last amended by Regulation (EC) No 1543/2007 (OJ L 337, 21.12.2007, p. 62). Jean-Luc Demarty"

    # print(dataFrame.head())
    similarityClass = similarity.textSimilarity()
    list = similarityClass.getCosineSimilarity(dataFrame, text, 4)

    #print(list)
    for index, score in list:
        document = dataFrame.iloc[[index - 1]]['docId'].iloc[0]
        print(document)
        #print("Document:" + dataFrame.iloc[[index - 1]]['docId'])
        #print("Document:" + dataFrame.iloc[[index -1]] + "; Score:" + str(score))
    # print(dataFrame.describe())
    print(dataFrame.columns)
    # print(dataFrame.info())
    logging.info('Finished:' + str(time.time() - start))
    # mongoConnection = dataBase.MongoConnection()
    # mongoConnection.checkDatabase()

if __name__ == '__main__':
    main()