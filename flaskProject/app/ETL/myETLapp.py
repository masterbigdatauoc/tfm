import logging, time
import fileManager

def main():
    '''
    Function to load files into MongoDb database
    :return:
    '''
    logging.basicConfig(filename='../../../log/myapp.log', level=logging.INFO)
    start = time.time()
    logging.info('Started:' + str(start))
    test = True
    fileManagerClass = fileManager.FileManager(test)
    fileManagerClass.myMultiprocessing()


    logging.info('Finished:'+ str(time.time()-start))

if __name__ == '__main__':
    main()