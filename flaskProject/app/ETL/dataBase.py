import logging
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError,ConnectionFailure,PyMongoError
import pandas as pd
#from pprint import pprint
# connect to MongoDB, change the << MONGODB URL >> to reflect your own connection string

class MongoConnection():
    def __init__(self,host="localhost",port=27017, test=False):
        self.host = host
        self.port = port
        self.test = test
        try:
            self.client = MongoClient(host=self.host, port=self.port, serverSelectionTimeoutMS=20000)
            self.db = self.client.storeddocumentscloud
            if (not self.test):
                self.collection = self.db.documents
            else:
                self.collection = self.db.documentsTest
            #print("Connected successfully!!!")
        except PyMongoError as e:
            print("Could not connect to MongoDB: %s") % e
            logging.info("Could not connect to MongoDB %s" % e)
        except ConnectionFailure as e:
            print("Could not connect to MongoDB: %s") % e
        except ServerSelectionTimeoutError:
            print("server is down.")
            logging.info("Could not connect to MongoDB")

# Issue the serverStatus command and print the results
#serverStatusResult=db.command("serverStatus")
#pprint(serverStatusResult)

    def insertDocument(self, param):
        self.collection.insert(param)

    def checkDatabase(self):
        cursor = self.collection.find()
        for document in cursor:
            logging.info("docId:" + str(document["docId"]) + " year" + str(document["year"]))

    def getRecords(self):
        dataFrame = pd.DataFrame(list(self.collection.find()))
        return dataFrame

    def getRecords2Compare(self):
       dataFrame = pd.DataFrame(list(self.collection.find({},{
            "_id":0,
            "docId":1,
            "fullText":1
        })))
       return dataFrame