from flask_login import UserMixin
from flask import current_app
from flask_mongoengine import MongoEngine
from app import login_manager

db = MongoEngine()

class DocStored(db.Document):
    '''class for define structure of each document'''
    docId = db.StringField(required = True)
    language = db.StringField(required = True)
    year = db.IntField(required=True)
    fullText = db.StringField(required=True)

    meta = {
        'collection': 'documentsTest',  # collection name
        'ordering': ['-score'],  # default ordering
        'auto_create_index': False,  # MongoEngine will not create index
    }

class User(UserMixin,db.Document):
    meta = {'collection': 'Users'}
    email = db.StringField(max_length=30)
    password = db.StringField()

@login_manager.user_loader
def load_user(user_id):
    return User.objects(pk=user_id).first()
