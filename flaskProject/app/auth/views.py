from flask import flash, redirect, render_template, url_for, current_app as app
from flask_login import login_required, login_user, logout_user
from werkzeug.security import generate_password_hash, check_password_hash

from . import auth_bp
from .forms import LoginForm, RegistrationForm
from ..models import User

@auth_bp.route('/register', methods=['GET','POST'])
def register():
    '''
    Handle requests to the /register route
    Add an user to the database through the registration form
    :return:
    '''
    form = RegistrationForm()
    if form.validate_on_submit():
        #Add the user to the database
        User(email =  form.email.data,
             password = generate_password_hash(form.password.data, method='sha256')).save()

        flash('You have successfully registered! You may now login.')
        return redirect((url_for('auth.login')))

    return render_template('auth/register.html',form = form, title = 'Register')

@auth_bp.route("/login", methods=['GET', 'POST'])
def login():
    '''
    Handle requests to the /login route
    Log an user in through the login form
    :return:
    '''
    form = LoginForm()

    if form.validate_on_submit():
        # check whether employee exists in the database and whether
        # the user entered matches the password in the database
        check_user = User.objects(email= form.email.data).first()
        if check_user:
            if check_password_hash(check_user['password'],form.password.data):
                login_user(check_user)
                # redirect to the dashboard page after login
                return redirect(url_for('home.dashboard'))
            else:
                flash('Invalid email or password.')
    # load login template
    return render_template('auth/login.html', form=form, title='Login')

@auth_bp.route("/logout")
@login_required
def logout():
    '''
    Handle requests to the /logout route
    Log an user out through the logout link
    :return:
    '''
    logout_user()
    flash('You have succcesfully been logged out.')

    return redirect(url_for('auth.login'))