from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, EqualTo
from ..models import User

class RegistrationForm(FlaskForm):
    '''Form for users to create new account'''
    email = StringField('Email',validators=[DataRequired(),Email(message='Invalid email')])
    password = PasswordField('Password', validators=[DataRequired(),EqualTo('confirm_password')])
    confirm_password=PasswordField('Confirm Password')
    submit =  SubmitField('Register')

    def validate_email(self,field):
        existing_user = User.objects(email=field.data).first()
        if existing_user is not None:
            raise ValidationError('Email is already in use.')


class LoginForm(FlaskForm):
    """
    Form for users to login
    """
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')