$(document).ready(function(){
 var interval = null;
 var numDocs = 0;
 var results = [];
  function blinker() {
    $(".doc-info").fadeOut(500);
    $(".doc-info").fadeIn(500);
  }

  function loading(){
    interval = setInterval(blinker,1000);
    $(".doc-info").text("Loading documents");
    $(".shadow").show();
    $(".doc-info").show();
  }

  function hide(numDocs) {
    clearInterval(interval);
    $(".shadow").hide();
    $(".doc-info").text("Documents loaded:" + numDocs);
    $(".doc-info").show();
  }

  $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", $csrf_token);
            }
        }
    });

  $(function() {
      $('input#submit').bind('click',function(){

        var data2Send = {
            country : $('#country').val(),
            env: $('#env').val(),
            year: $('#year').val()
        }
        loading();
        $.ajax({
            url: $linkLoadDocs,
            data: data2Send,
            type: 'POST',
            success: function(response) {
                //console.log(response);
                numDocs = response;
                hide(numDocs);
            },
            error: function(error) {
                $(".shadow").hide();
                console.log(error);
            }
        });
    });

    $('input#findSim').bind('click',function(){
      currentVal = $('input#findSim').val();
      $('input#findSim').val("Find Similarity");

      if (currentVal == 'Find Similarity'){
        if (numDocs == 0){
        alert('Documents should be loaded');
        } else {
           $(".shadow").show();
           $("#hiddenText").hide();
           $("div .dashboard-content-left").hide();
            var data2Send = $('.txta-small').val();
            data2Send = {text2Compare: data2Send};
            $.ajax({
                url: $linkSimilarity,
                data: data2Send,
                type: 'POST',
                //dataType: 'json',
                success: function(response) {
                    //console.log(JSON.stringify(response.resultsArray.length));
                    results = response.resultsArray;
                    $(".shadow").hide();
                    var $tableDiv =$('.Table');
                    $tableDiv.empty();
                    var $headerRow = $('<div class="Table-header"><div class="Table-row-item">Document Id</div><div class="Table-row-item">Similarity %</div><div class="Table-row-item">Text</div><div class="Table-row-item">Compare</div></div>')
                    .appendTo($tableDiv);
                    results.forEach(function(result){
                        var $row = $('<div class="Table-row">')
                         .appendTo($tableDiv);
                       // iterate over the buses
                       $('<div class="Table-row-item" data-header="Header1">').text(result.docId).appendTo($row);
                       $('<div class="Table-row-item" data-header="Header1">').text(result.score).appendTo($row);
                       $('<div class="Table-row-item" data-header="Header1">').text(result.text.substring(1,50) + '...').appendTo($row);
                       $('<div class="Table-row-item" data-header="Header4"><img class="eye" id="' + result.index + '" src="../static/img/icon-eye.png" /></div>').appendTo($row)
                    });
                    $("div .dashboard-content-left").show();
                },
                error: function(error) {
                    $(".shadow").hide();
                    console.log(error);
                }
            });
        }
      } else{
        $('#hiddenText').hide();
        $("div .dashboard-content-left").show();
      }

    });

    $('#env').bind('change',function(){
        var envSelected = $(this).find("option:selected").attr('value');
        if (envSelected == 'prod'){
            $('#year').show();
        } else {
            $('#year').hide();
        }
    });

    $('#setText').bind('change', function(){
        if (this.checked){
            $("textarea#t1").text('Decision of the EEA Joint Committee of 8 June 2004 amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement THE EEA JOINT COMMITTEE, Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as ‘the Agreement’, and in particular Article 98 thereof, Whereas: Annex II to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg [1]. Directive 2003/15/EC of the European Parliament and of the Council of 27 February 2003 amending Council Directive 76/768/EEC on the approximation of the laws of the Member States relating to cosmetic products [2] is to be incorporated into the Agreement, HAS DECIDED AS FOLLOWS: Article 1 The following indent shall be added in point 1 (Council Directive 76/768/EEC) of Chapter XVI of Annex II to the Agreement: Directive 2003/15/EC of the European Parliament and of the Council of 27 February 2003 (OJ L 66, 11.3.2003, p. 26).’ Article 2 The texts of Directive 2003/15/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic. Article 3 This Decision shall enter into force on 9 June 2004, provided that all the notifications pursuant to Article 103(1) of the Agreement have been made to the EEA Joint Committee [3]. Article 4 This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union. Done at Brussels, 8 June 2004. For the EEA Joint Committee The President Gillespie OJ L 130, 29.4.2004, p. 3. OJ L 66, 11.3.2003, p. 26. No constitutional requirements indicated');
        } else {
            $("textarea#t1").text('Paste your text and click on Find Similarity');
        }
    });
  });

  $('.Table').on('click', '.eye', function(event){
    // do something here
    //alert(event.target.id);
    var index2Show = event.target.id;
    var result2Show = results.filter(function(el){
        return el.index == index2Show;
    });
    console.log(result2Show);
    $("textarea#t2").text(result2Show[0].text);
    $('#hiddenText').show();
    $("div .dashboard-content-left").hide();
    $('input#findSim').val("Back to results");

  });

});


