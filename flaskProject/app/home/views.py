from flask import render_template, json, request,jsonify
from mongoengine.context_managers import switch_collection
import pandas as pd
from flask_login import login_required
from .forms import SelectCountryForm

from . import home_bp
from ..models import User, DocStored
from ..ETL import similarity

df = pd.DataFrame()

@home_bp.route("/")
def homepage():
    """
    Render the homepage template on the / route
    :return:
    """
    return render_template("home/index.html",title="Welcome")

@home_bp.route("/dashboard", methods=['GET','POST'])
@login_required
def dashboard():
     """
     Render the dashboard template on the dashboard route
     :return:
     """
     form = SelectCountryForm()
     return render_template("home/dashboard.html",form = form, title="Dashboard")


@home_bp.route("/loadDocs", methods=['GET', 'POST'])
@login_required
def loadDocs():
     '''
     Load documents from database
     :return:
     '''
     #test = True
     try:
         if request.method == "POST":
             if (request.form.get('env') == "test"):
                 with switch_collection(DocStored, 'documentsTest') as Doc:
                     #return request.form.get('country')
                     #allDocs = json.dumps(Doc.objects())
                     allDocs = json.dumps(Doc.objects(language=request.form.get('country')))
             else:
                 with switch_collection(DocStored, 'documents') as Doc:
                     #allDocs = json.dumps(Doc.objects(language=request.form.get('country'))[:15])
                     year2Search = int(request.form.get('year'))
                     allDocs = json.dumps(Doc.objects(language=request.form.get('country'), year = year2Search))

             global df
             df = pd.read_json(allDocs)
             count_row = df.shape[0]
             return str(count_row)
                    #+ str(request.form.get('env') )
         else:
             return str(-2)
     except Exception as e:
        return "Error \n %s" % (e)


@home_bp.route("/findSimilarity", methods=['GET', 'POST'])
@login_required
def findSimilarity():
     '''
     Find similarity between documents
     :return: Array of similar documents
     '''
     global df
     try:
         if request.method == "POST":
             if request.form.get('text2Compare'):
                 simTool = similarity.textSimilarity()
                 arrResults = simTool.getCosineText(df,request.form.get('text2Compare'),4)
                 return jsonify(resultsArray=arrResults)
             else:
                 return 'Wrong parameters'
         else:
             return "Wrong method"
     except (ValueError, KeyError, TypeError):
         return str(ValueError) + str(KeyError) + str(TypeError)


@home_bp.route("/checkDocuments", methods=['GET', 'POST'])
@login_required
def checkDocuments():
     '''
     show a screen with two documents
     :return:
     '''
     return render_template("home/checkDocuments.html", title="CheckingDocs", text1="Not valid solutions", text2="text2")