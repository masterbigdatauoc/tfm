from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField

class SelectCountryForm(FlaskForm):
    country =  SelectField(u"Select Country",choices = [("EN-GB","English")])
    submit = SubmitField("Load Documents")

