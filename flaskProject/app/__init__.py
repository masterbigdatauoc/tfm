import os

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_mongoengine import MongoEngine
from flask_debugtoolbar import DebugToolbarExtension
from flask_wtf.csrf import CSRFProtect

import config
## include db name in URI; _HOST entry overwrites all others

login_manager = LoginManager()

csrf = CSRFProtect()

def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config['DEBUG_TB_PANELS'] = ['flask_mongoengine.panels.MongoDebugPanel']


    #app.config['MONGODB_SETTINGS'] = {'host':config.Config.MONGODB_HOST_LOCAL}
    app.config['MONGODB_SETTINGS'] = {'db': config.Config.MONGODB_DATABASE,
                                      'host': config.Config.MONGODB_HOST,
                                      'port': config.Config.MONGODB_PORT,
                                      'username': config.Config.MONGODB_USERNAME,
                                      'password': config.Config.MONGODB_PASSWORD
                                      }
    app.config['SECRET_KEY'] = config.Config.SECRET_KEY

    #It connects but no replicaset
    app.debug = False
    # initalize app with database
    Bootstrap(app)

    #initialize protecction
    csrf.init_app(app)
    from .models import db
    db.init_app(app)
    DebugToolbarExtension(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"


    #if test_config is None:
        #load the instance config, if it exists, when no testing
    #    app.config.from_pyfile('config.py', silent=True)
    #else:
        #load the test config if passed in
        #app.config.from_mapping(test_config)


    from app import models
    
    from app.ETL import sim_bp as sim_bp, db_bp as db_bp
    from .auth import auth_bp as auth_blueprint
    from .home import home_bp as home_blueprint
    app.register_blueprint(sim_bp)
    app.register_blueprint(db_bp)
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(home_blueprint)


    return app




