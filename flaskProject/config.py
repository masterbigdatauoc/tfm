# config.py
class Config():
    """
    Common configurations
    """
    SECRET_KEY = "T\xea\xdc:O\xf6\xab\xfd\xb5\x0e\x19\x94\x90\xd9\x88OY<X\xc2\xd9\xc0O%"
    #MONGODB_HOST = "mongodb://flaskUser:B4t3k4g1@tfmcluster-shard-00-00-ejih2.mongodb.net:27017,tfmcluster-shard-00-01-ejih2.mongodb.net:27017,tfmcluster-shard-00-02-ejih2.mongodb.net:27017/storeddocumentscloud?ssl=true&replicaSet=TFMCLuster-shard-0&authSource=admin"
    MONGODB_HOST = "mongodb://flaskUser:B4t3k4g1@ds127634.mlab.com:27634/storeddocumentscloud"
    MONGODB_DATABASE = "storeddocumentscloud"
    MONGODB_USERNAME = "flaskUser"
    MONGODB_PASSWORD = "B4t3k4g1"
    MONGODB_PORT = 27017
    # Put any configurations here that are common across all environments
    MONGODB_HOST_LOCAL = 'mongodb://localhost:27017/storeddocumentscloud'
    MONGODB_DATABASE = "storeddocumentscloud"



class DevelopmentConfig(Config):
    """
    """

    DEBUG = True


class ProductionConfig(Config):
    """
    Production configurations
    """

    DEBUG = False

app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}


